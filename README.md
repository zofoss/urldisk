# [💾 urlDISK](https://urldisk.zofoss.org/)

- Share small files without a cloud 🤩
- Tool to store files in URL, No Backend! No Database!
- This tool Converts file into base64. You can then copy the URL to this site to share files. 

## Credits

We have taken reference from the following:
- [W3Schools Copy Text Function](https://www.w3schools.com/howto/tryit.asp?filename=tryhow_js_copy_clipboard)
- [Stack Overflow Function to convert file into base64](https://stackoverflow.com/questions/6150289)
- [Stack Overflow Input Styling](https://stackoverflow.com/questions/572768)

## License

This tool is licensed under the MIT License. See [LICENSE](https://gitlab.com/zofoss/urldisk/-/blob/main/LICENSE) for details.
